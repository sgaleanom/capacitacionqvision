package com.steps;

import com.pages.DataTableSearchPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class DataTableSearchStep extends ScenarioSteps {

  @Page
  DataTableSearchPage dataTableSearchPage;

  @Step
  public void filtrarDataTable(String filtro) {
    dataTableSearchPage.filtrarTabla(filtro);
  }

  @Step
  public void validarResgistroTabla(String filtro) {
    MatcherAssert.assertThat("El registro " + filtro + "no fue encontrado",
        dataTableSearchPage.validarExistenciaRegistroTabla(filtro));
  }

  @Step
  public void validarInexistenciaRegistroTabla(String mensaje) {
    MatcherAssert.assertThat("El registro fue encontrado dentro de la tabla",
        dataTableSearchPage.validarExistenciaRegistroTabla(mensaje));
  }
}
