package com.steps;

import com.pages.SimpleFormDemoPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class SimpleFormDemoStep extends ScenarioSteps {

  @Page
  SimpleFormDemoPage simpleFormDemoPage;

  @Step
  public void diligenciarMensajePantalla(String mensaje) {
    simpleFormDemoPage.escribirMensaje(mensaje);
    simpleFormDemoPage.mostrarMensaje();
  }

  @Step
  public void validarMensaje(String mensaje) {
    MatcherAssert.assertThat("El mensaje mostrado en pantalla es diferente al esperado",
        simpleFormDemoPage.capturarMensajeResultado().equals(mensaje));
  }

  @Step
  public void sumarValores(String valorA, String valorB) {
    simpleFormDemoPage.ingresarValorSumaA(valorA);
    simpleFormDemoPage.ingresarValorSumaB(valorB);

    simpleFormDemoPage.mostrarSuma();
  }

  @Step
  public void validarResultadoSuma(String resultadoSuma) {
    MatcherAssert.assertThat("El resultado de la suma diferente al esperado " + simpleFormDemoPage
            .capturarResultadoSuma() + " != " + resultadoSuma,
        simpleFormDemoPage.capturarResultadoSuma().equals(resultadoSuma));
  }
}
