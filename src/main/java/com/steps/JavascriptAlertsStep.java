package com.steps;

import com.pages.JavascriptAlertsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class JavascriptAlertsStep extends ScenarioSteps {

  @Page
  JavascriptAlertsPage javascriptAlertsPage;

  @Step
  public void ingresarMensajeJavascript(String mensaje) {
    javascriptAlertsPage.ingresarMensaje(mensaje);
  }

  @Step
  public void validarMensajeIngresado(String mensaje) {
    MatcherAssert.assertThat("El mensaje ingresado no aparece en pantalla ",
        javascriptAlertsPage.obtenerMensaje().contains(mensaje));
  }
}
