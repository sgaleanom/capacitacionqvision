package com.steps;

import com.pages.RadioButtonsDemoPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class RadioButtonDemoStep extends ScenarioSteps {

  @Page
  RadioButtonsDemoPage radioButtonsDemoPage;

  @Step
  public void seleccionarRadioButton(String genero) {
    radioButtonsDemoPage.seleccionarRadioButton(genero);
    radioButtonsDemoPage.validarRadioButton();
  }

  @Step
  public void validarRadioButtonSeleccionado(String genero) {
    MatcherAssert.assertThat("El la selección de radio button es incorrecta",
        radioButtonsDemoPage.capturarMensajeRadioButtonSeleccionado().contains(genero));
    MatcherAssert.assertThat("La propiedad del radio button es diferente a seleccionada",
        radioButtonsDemoPage.validarRadioButtonSeleccionado(genero));
  }
}
