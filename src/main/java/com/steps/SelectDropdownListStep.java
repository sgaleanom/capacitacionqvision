package com.steps;

import com.pages.SelectDropdownListPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class SelectDropdownListStep extends ScenarioSteps {

  @Page
  SelectDropdownListPage selectDropdownListPage;

  @Step
  public void seleccionarDia(String dia) {
    selectDropdownListPage.seleccionarDiaLista(dia);
  }

  @Step
  public void validarDiaSeleccionado(String dia) {
    MatcherAssert.assertThat(
        "El mensaje mostrado no es el esperado " + dia + "!=" + selectDropdownListPage
            .obtenerMensajeResultado(),
        selectDropdownListPage.obtenerMensajeResultado().contains(dia));
  }
}
