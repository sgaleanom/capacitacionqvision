package com.steps;

import com.pages.JQuerySelectDropdownPage;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class JQuerySelectDropdownStep extends ScenarioSteps {

  @Page
  JQuerySelectDropdownPage jquerySelectDropdownPage;

  public void ingresarBusquedaEstado(String lenguaje) {
    jquerySelectDropdownPage.seleccionarLenguaje(lenguaje);
  }

  public void ingresarBusquedaPais(String pais) {
    jquerySelectDropdownPage.seleccionarPaisLista(pais);
  }

  public void validarBusquedaEstado(String lenguaje) {
    MatcherAssert.assertThat(
        "El elemento seleccionado no es el correcto " + lenguaje + " != " + jquerySelectDropdownPage
            .obtenerLenguajeSeleccionado(),
        jquerySelectDropdownPage.obtenerLenguajeSeleccionado().equals(lenguaje));
  }

  public void validarBusquedaPais(String pais) {
    MatcherAssert.assertThat(
        "El elemento seleccionado no es el correcto " + pais + " != " + jquerySelectDropdownPage
            .obtenerLenguajeSeleccionado(),
        jquerySelectDropdownPage.obtenerPaisSeleccionado().equals(pais));
  }
}
