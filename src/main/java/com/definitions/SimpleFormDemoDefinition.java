package com.definitions;

import com.steps.SimpleFormDemoStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class SimpleFormDemoDefinition {

  @Steps
  SimpleFormDemoStep simpleFormDemoStep;

  private static final String MENSAJE_ASERCION = "Mensaje asercion";
  private static final String MENSAJE_RESULTADO_SUMA = "Resultado suma";

  @Cuando("^ingreso el mensaje: (.*)$")
  public void ingresarMensaje(String mensaje) {
    Serenity.setSessionVariable(MENSAJE_ASERCION).to(mensaje);
    simpleFormDemoStep.diligenciarMensajePantalla(mensaje);
  }

  @Entonces("^valido que el mensaje se muestre en pantalla$")
  public void validarMensaje() {
    simpleFormDemoStep.validarMensaje(Serenity.sessionVariableCalled(MENSAJE_ASERCION));
  }

  @Cuando("^sumo (.*) \\+ (.*)$")
  public void sumarValores(String valorA, String valorB) {
    Serenity.setSessionVariable(MENSAJE_RESULTADO_SUMA)
        .to(Integer.parseInt(valorA) + Integer.parseInt(valorB));
    simpleFormDemoStep.sumarValores(valorA, valorB);
  }

  @Entonces("^valido el resultado correcto de la suma$")
  public void validarResultadoSuma() {
    simpleFormDemoStep
        .validarResultadoSuma(Serenity.sessionVariableCalled(MENSAJE_RESULTADO_SUMA).toString());
  }
}
