package com.definitions;

import com.steps.JavascriptAlertsStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class JavascriptAlertsDefinition {

  private static final String MENSAJE = "mensaje";

  @Steps
  JavascriptAlertsStep javascriptAlertsStep;

  @Cuando("^Ingreso el mensaje en el JavaScript: (.*)$")
  public void ingresarMensaje(String mensaje){
    Serenity.setSessionVariable(MENSAJE).to(mensaje);
    javascriptAlertsStep.ingresarMensajeJavascript(mensaje);
  }

  @Entonces("^valido el mensaje en pantalla$")
  public void validarMensaje(){
    javascriptAlertsStep.validarMensajeIngresado(Serenity.sessionVariableCalled(MENSAJE));
  }
}
