package com.definitions;

import com.steps.JQuerySelectDropdownStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class JQuerySelectDropdownDefinition {

  @Steps
  JQuerySelectDropdownStep jQuerySelectDropdownStep;

  private static final String ASERCION = "asercion";

  @Cuando("^ingreso la palabra de busqueda (.*)$")
  public void ingresarEstadoBusqueda(String lenguaje) {
    Serenity.setSessionVariable(ASERCION).to(lenguaje);
    jQuerySelectDropdownStep.ingresarBusquedaEstado(lenguaje);
  }

  @Entonces("^valido que el elemento haya sido encontrado$")
  public void validarLenguajeEncontrado() {
    jQuerySelectDropdownStep.validarBusquedaEstado(Serenity.sessionVariableCalled(ASERCION));
  }

  @Cuando("^ingreso el pais de busqueda (.*)$")
  public void ingresarPaisBusqueda(String pais) {
    Serenity.setSessionVariable(ASERCION).to(pais);
    jQuerySelectDropdownStep.ingresarBusquedaPais(pais);
  }


  @Entonces("^valido que el elemento pais haya sido encontrado$")
  public void validarBusquedaPais() {
    jQuerySelectDropdownStep.validarBusquedaPais(Serenity.sessionVariableCalled(ASERCION));
  }
}
