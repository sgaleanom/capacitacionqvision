package com.definitions;

import com.steps.RadioButtonDemoStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;

public class RadioButtonDemoDefinition {

  @Steps
  RadioButtonDemoStep radioButtonDemoStep;

  @Cuando("^selecciono el radio button (.*)$")
  public void seleccionarRadioButton(String genero) {
    radioButtonDemoStep.seleccionarRadioButton(genero);
  }

  @Entonces("^valido la selección correcta del radio button (.*)$")
  public void validarSeleccionRadioButton(String genero) {
    radioButtonDemoStep.validarRadioButtonSeleccionado(genero);
  }
}
