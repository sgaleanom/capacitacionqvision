package com.definitions;

import com.steps.DataTableSearchStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class DataTableSearchDefinition {

  @Steps
  DataTableSearchStep dataTableSearchStep;

  private static final String FILTRO = "filtro";

  @Cuando("^filtro los registros de la tabla mediante las palabras (.*)$")
  public void filtrarTabla(String filtro) {
    Serenity.setSessionVariable(FILTRO).to(filtro);
    dataTableSearchStep.filtrarDataTable(filtro);
  }

  @Entonces("^valido que el dato se encuentre en la tabla$")
  public void validarExistenciaDato() {
    dataTableSearchStep.validarResgistroTabla(Serenity.sessionVariableCalled(FILTRO));
  }

  @Entonces("^valido que el dato no se encuentre en la tabla mediante el mensaje (.*)$")
  public void validarInexistenciaDato(String mensaje) {
    dataTableSearchStep.validarInexistenciaRegistroTabla(mensaje);
  }
}
