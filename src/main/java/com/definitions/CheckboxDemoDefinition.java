package com.definitions;

import com.steps.CheckboxDemoStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;

public class CheckboxDemoDefinition {

  @Steps
  CheckboxDemoStep checkboxDemoStep;

  @Cuando("^marco el checkbox$")
  public void marcarCheckbox() {
    checkboxDemoStep.marcarCheckbox();
  }

  @Entonces("^valido el mensaje en pantalla (.*)$")
  public void validarMensajeCheckbox(String mensajeEsperado) {
    checkboxDemoStep.validarCheckboxMarcado(mensajeEsperado);
  }
}
