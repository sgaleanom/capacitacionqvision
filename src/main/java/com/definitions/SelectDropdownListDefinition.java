package com.definitions;

import com.steps.SelectDropdownListStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;

public class SelectDropdownListDefinition {

  @Steps
  SelectDropdownListStep selectDropdownListStep;

  @Cuando("^selecciono un día de la semana (.*)$")
  public void seleccionarElementoLista(String dia){
    selectDropdownListStep.seleccionarDia(dia);
  }

  @Entonces("^valido la selección correcta del día en la lista (.*)$")
  public void validarDiaSeleccionado(String dia) {
    selectDropdownListStep.validarDiaSeleccionado(dia);
  }
}
