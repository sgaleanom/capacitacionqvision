package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SimpleFormDemoPage extends PageObject {

  @FindBy(id = "user-message")
  private WebElementFacade txtMensaje;

  @FindBy(xpath = "//button[@class='btn btn-default' and text()='Show Message']")
  private WebElementFacade btnMostrarMensaje;

  @FindBy(id = "display")
  private WebElementFacade lblMensajeResultado;

  @FindBy(id = "sum1")
  private WebElementFacade txtValorSumaA;

  @FindBy(id = "sum2")
  private WebElementFacade txtValorSumaB;

  @FindBy(xpath = "//button[@class='btn btn-default' and text()='Get Total']")
  private WebElementFacade btnMostrarResultadoSuma;

  @FindBy(id = "displayvalue")
  private WebElementFacade lblResultadoSuma;

  public void escribirMensaje(String mensaje) {
    txtMensaje.clear();
    txtMensaje.sendKeys(mensaje);
  }

  public void mostrarMensaje() {
    btnMostrarMensaje.click();
  }

  public String capturarMensajeResultado() {
    return lblMensajeResultado.getText();
  }

  public void ingresarValorSumaA(String valor) {
    txtValorSumaA.clear();
    txtValorSumaA.sendKeys(valor);
  }

  public void ingresarValorSumaB(String valor) {
    txtValorSumaB.clear();
    txtValorSumaB.sendKeys(valor);
  }

  public void mostrarSuma() {
    btnMostrarResultadoSuma.click();
  }

  public String capturarResultadoSuma() {
    return lblResultadoSuma.getText();
  }
}
