package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class DataTableSearchPage extends PageObject {

  @FindBy(id="task-table-filter")
  private WebElementFacade txtFiltroTabla;

  @FindBy(id="task-table")
  private WebElementFacade tblTablaTareas;

  public void filtrarTabla(String filtro){
    txtFiltroTabla.sendKeys(filtro);
  }

  public boolean validarExistenciaRegistroTabla(String filtro) {
    return tblTablaTareas.find(By.xpath("//td[text()= '" + filtro + "']")).isVisible();
  }
}
