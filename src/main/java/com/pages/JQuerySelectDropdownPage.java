package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.Keys;

public class JQuerySelectDropdownPage extends PageObject {

  @FindBy(id = "files")
  private WebElementFacade cmbLenguajes;

  @FindBy(className = "select2-selection__arrow")
  private WebElementFacade cmbBusquedaPais;

  @FindBy(xpath = "//span[@class='select2-search select2-search--dropdown']//input[@class='select2-search__field']")
  private WebElementFacade txtFiltroBusquedaPais;

  @FindBy(id = "select2-country-container")
  private WebElementFacade cmbPais;

  public void seleccionarLenguaje(String lenguaje) {
    cmbLenguajes.selectByVisibleText(lenguaje);
  }

  public String obtenerLenguajeSeleccionado(){
    return cmbLenguajes.getSelectedVisibleTextValue();
  }

  public void seleccionarPaisLista(String pais){
    cmbBusquedaPais.click();
    txtFiltroBusquedaPais.sendKeys(pais, Keys.ENTER);
  }

  public String obtenerPaisSeleccionado(){
    return cmbPais.getText();
  }
}
