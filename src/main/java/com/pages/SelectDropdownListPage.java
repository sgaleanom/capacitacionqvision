package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SelectDropdownListPage extends PageObject {

  @FindBy(id = "select-demo")
  private WebElementFacade cmbListaDias;

  @FindBy(className = "selected-value")
  private WebElementFacade lblMensajeResultadoSeleccionDia;

  public void seleccionarDiaLista(String dia) {
    cmbListaDias.selectByVisibleText(dia);
  }

  public String obtenerMensajeResultado() {
    return lblMensajeResultadoSeleccionDia.getText();
  }
}
