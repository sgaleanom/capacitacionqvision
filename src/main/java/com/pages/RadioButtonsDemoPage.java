package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class RadioButtonsDemoPage extends PageObject {

  @FindBy(xpath = "//div[@class='panel panel-default']//div[@class='panel-body']//p[text()='Click on button to get the selected value']//..")
  private WebElementFacade containerRadioButtons;

  @FindBy(id = "buttoncheck")
  private WebElementFacade btnValidarRadioButton;

  @FindBy(className = "radiobutton")
  private WebElementFacade lblResultadoSeleccionRadioButton;

  public void seleccionarRadioButton(String genero) {
    containerRadioButtons.find(By.xpath("//input[@value='" + genero + "']")).click();
  }

  public void validarRadioButton() {
    btnValidarRadioButton.click();
  }

  public String capturarMensajeRadioButtonSeleccionado() {
    return lblResultadoSeleccionRadioButton.getText();
  }

  public boolean validarRadioButtonSeleccionado(String genero) {
    return containerRadioButtons.find(By.xpath("//input[@value='" + genero + "']")).isSelected();
  }
}
