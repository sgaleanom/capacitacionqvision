package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class JavascriptAlertsPage extends PageObject {

  @FindBy(id = "prompt-demo")
  private WebElementFacade lblMensaje;

  @FindBy(xpath = "//button[@class=\"btn btn-default btn-lg\" and text()='Click for Prompt Box']")
  private WebElementFacade btnIngresarMensaje;

  public void ingresarMensaje(String mensaje) {
    btnIngresarMensaje.click();
    getDriver().switchTo().alert().sendKeys(mensaje);
    getDriver().switchTo().alert().accept();
  }

  public String obtenerMensaje() {
    return lblMensaje.getText();
  }
}
