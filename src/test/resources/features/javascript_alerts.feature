#language: es

Característica: Como usuario quiero validar el texto ingresado mediante una alerta JavaScript

  Escenario: Ingresar un mensaje en una alerta JavaScript y validarlo
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Alerts & Modals/Javascript Alerts
    Cuando Ingreso el mensaje en el JavaScript: Mensaje prueba
    Entonces valido el mensaje en pantalla
