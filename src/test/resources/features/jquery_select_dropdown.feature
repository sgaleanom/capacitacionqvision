#language: es

Característica: Como usuario quiero ingresar un valor de busqueda en un listado

  Escenario: Ingresar un lenguaje dentro de un cuadro de busqueda y validar su selección
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Input Forms/JQuery Select dropdown
    Cuando ingreso la palabra de busqueda C
    Entonces valido que el elemento haya sido encontrado

  Escenario: Ingresar un pais dentro de un cuadro de busqueda y validar su selección
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Input Forms/JQuery Select dropdown
    Cuando ingreso el pais de busqueda Australia
    Entonces valido que el elemento pais haya sido encontrado