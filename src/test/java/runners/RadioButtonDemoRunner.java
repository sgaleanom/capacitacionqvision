package runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    features =
        "src/test/resources/features/radio_buttons_demo.feature",
    glue = {"com.definitions"}
)
public class RadioButtonDemoRunner {

}
