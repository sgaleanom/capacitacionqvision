#language: es

Característica: Como usuario quiero validar la selección de un radio button

  Esquema del escenario: seleccionar un rabio button de genero y validar su selección
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Input Forms/Radio Buttons Demo
    Cuando selecciono el radio button <genero>
    Entonces valido la selección correcta del radio button <genero>
    Ejemplos:
      | genero |
      | Male   |
      | Female |