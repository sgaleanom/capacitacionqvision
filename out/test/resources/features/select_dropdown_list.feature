#language: es

Característica: Como usuario quiero validar la selección de un dia de la semana dentro de una lista

  Esquema del escenario: seleccionar un dia de una lista y validar su selección
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Input Forms/Select Dropdown List
    Cuando selecciono un día de la semana <dia>
    Entonces valido la selección correcta del día en la lista <dia>
    Ejemplos:
      | dia      |
      | Sunday   |
      | Monday   |
      | Saturday |