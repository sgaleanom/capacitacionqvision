#language: es

Característica: Como usuario quiero ingresar un mensaje para posteriormente ser mostrado en pantalla
  y calcular la suma de dos valores

  Escenario: Ingresar un mensaje en un campo texto y validar que se muestre en pantalla
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Input Forms/Simple Form Demo
    Cuando ingreso el mensaje: esto es un mensaje prueba
    Entonces valido que el mensaje se muestre en pantalla

  Esquema del escenario: Ingresar dos valores y validar el resultado de la suma
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Input Forms/Simple Form Demo
    Cuando sumo <valor A> + <valor B>
    Entonces valido el resultado correcto de la suma
    Ejemplos:
    |valor A | valor B|
    |5       |13      |
    |3       |11      |

