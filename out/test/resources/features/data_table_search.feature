#language: es

Característica: Como usuario quiero validar la aplicación de filtros y validación de valores dentro de una tabla

  Escenario: filtrar datos y validar existencia
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Table/Table Data Search
    Cuando filtro los registros de la tabla mediante las palabras SEO tags
    Entonces valido que el dato se encuentre en la tabla

  Escenario: filtrar datos y validar existencia
    Dado que ingreso a la pagina Selenium Easy y navego hasta el formualario Table/Table Data Search
    Cuando filtro los registros de la tabla mediante las palabras Bootstrap 2
    Entonces valido que el dato no se encuentre en la tabla mediante el mensaje No results found
